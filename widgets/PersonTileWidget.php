<?php
namespace app\widgets;

use Yii;
use app\controllers\MhController as MH;

class PersonTileWidget extends AbstractTileWidget {

    public function renderTitle(){
        return $this->tile->Title;
    }

    public function renderInfoText(){
        $personArray = explode('<br>',$this->tile->InfoText);

        $personData = '';
        if (!empty($personArray)) {
            foreach ($personArray as $person) {
                $personData .= '<li>'.$person.'</li>';
                //$personData .= '<li>' . Html::a($person->firstName . ' ' . $person->name, ['dynamic-page/index', 'uri' => $this->companyUri, 'detail' => \Yii::$app->uriGenerator->generatePersonUri($person)]) . '</li>';
            }
        }
        return '<ul>'.$personData.'</ul>';
    }

    public function renderTableHeader(){
        $head = $this->tile->ListHead;
        $header = '';
        if (!empty($head)) {
            $header .= '<div class="col-md-4 col-xs-6">'.$head->th1.'</div>';
            $header .= '<div class="col-md-4 col-xs-6">'.$head->th2.'</div>';
            $header .= '<div class="col-md-2 col-xs-6">'.$head->th3.'</div>';
            $header .= '<div class="col-md-2 col-xs-6">'.$head->th4.'</div>';
        }
        return $header;
    }

    public function renderTable(){
        $table = '';
        if(!empty($this->tile->ListValues)){
            foreach($this->tile->ListValues as $person){

                switch($this->open){
                    case 'v':
                        $functionality = $person->Funktion;
                        break;
                    default:
                        $functionality = $person->Unterschrift;
                        break;
                }

                if($person->Status == 1) {
                    $table .= '<a href="'.$this->createPersonLink($person->Person,$person->PersonId).'" class="ownersRow" rel="nofollow">
                                  <div href="#" class="col-md-4 col-xs-6"><span class="smallIcon ownerIconS"></span>' . $person->Person . '</div>
                                  <div href="#" class="col-md-4 col-xs-6">' . $functionality . '</div>
                                  <div href="#" class="col-md-2 col-xs-6">' . $person->Seit . '</div>
                                  <div href="#" class="col-md-2 col-xs-6">' . self::getPersonStatus($person->Status) . '</div>
                              </a>';
                }
            }
        }
        return $table;
    }

    private function createPersonLink($name,$PersonId){

        $name = strtolower($name);
        $name = str_replace(' ','_',$name);
        $name = str_replace('-','_',$name);
        $name = str_replace(['ä','ö','ü','Ä','Ö','Ü'],['a','o','u','A','O','U'],$name);

        //$personLink = 'http://dev-sm.moneyhouse.ch'.(MH::GetLanguage()!='de'?'/'.MH::GetLanguage():'').'/mobile/Beta?personid='.$PersonId;
        $personLink = 'http://www.moneyhouse.ch'.(MH::GetLanguage()!=''?'/'.MH::GetLanguage():'/de').'/mobile/Beta?personid='.$PersonId;
        return $personLink;
    }

    private function getPersonStatus($id){
        if($id=1)
            return Yii::t('person','aktiv');
        else
            return Yii::t('person','inaktiv');
    }
}