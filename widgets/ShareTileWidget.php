<?php
namespace app\widgets;

use Yii;

class ShareTileWidget extends AbstractTileWidget {

    public function renderTitle(){
        return $this->tile->Title;
    }

    public function renderInfoText(){
        return $this->tile->InfoText;
    }

    public function renderTableHeader(){
        $head = $this->tile->ListHead;
        $header = '';
        if (!empty($head)) {
            $header .= '<div class="col-xs-6">' . $head->th1 . '</div>';
            $header .= '<div class="col-xs-6">' . $head->th2 . '</div>';
        }
        return $header;
    }

    public function renderTable(){
        $table = '';
        if(!empty($this->tile->ListValues)){
            $table .= '<a class="ownersRow" rel="nofollow">';
            $table .= '    <div href="#" class="col-xs-6">'.$this->tile->ListValues[0]->Share.'</div>';
            $table .= '    <div href="#" class="col-xs-6">'.$this->tile->ListValues[0]->Period.'</div>';
            $table .= '</a>';
            /*foreach($this->tile->ListValues as $publication){

            }*/
        }
        return $table;
    }

}