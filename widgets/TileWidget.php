<?php
namespace app\widgets;

use Yii;
use yii\base\Widget;

class TileWidget extends Widget {

    public $company;
    public $tile;
    public $open;
    public $second;

    private $widgetconfig;

    public function run(){

        $html = '';

        $this->widgetconfig = [
            'tile'=>$this->tile,
            'company' => $this->company,
            'second' => $this->second,
            'open' => $this->open
        ];

        $tileCategory = (isset($this->open)?$this->open:$this->tile->Sub);

        switch ($tileCategory) {
            case 'p':
                $html = PersonTileWidget::widget($this->widgetconfig);
                break;
            case 'v':
                $html = PersonTileWidget::widget($this->widgetconfig);
                break;
            case 'pub':
                $html = ShabTileWidget::widget($this->widgetconfig);
                break;
            case 'fin':
                $html = ShareTileWidget::widget($this->widgetconfig);
                break;
            case 'ra':
                $html = RatingTileWidget::widget($this->widgetconfig);
                break;
        }

        echo $html;
    }
}