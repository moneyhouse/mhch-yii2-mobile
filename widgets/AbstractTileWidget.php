<?php
namespace app\widgets;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use app\controllers\MhController as MH;

abstract class AbstractTileWidget extends Widget {

    public $company;
    public $tile;
    public $open;
    public $second;
    public $lang;

    public function run(){
        if(isset($this->open)){
            $html = $this->getOpenTile();
        }else{
            $html = $this->getCloseTile();
        }

        echo $html;
    }

    public function getCloseTile() {
        if(($this->company->StatusCode == 0 OR $this->tile->Counter == '0K') AND $this->tile->Sub != 'pub') {
            $html = '';
        } else {
            $html = '<div class="infoBox col-md-6' . ($this->second ? ' secondBox' : '') . '">
                        <div class="infoBoxContainer"><div class="infoBoxLeft">';

            if(!is_null($this->tile->Counter)) {
                $html .= '<div class="infoIcon ' . $this->tile->Image . '"></div>';
            }

            $html .= '<div class="infoTitle">'.$this->handleTitleSize($this->renderTitle()).'</div>';

            if(!is_null($this->tile->Counter)) {
                $html .= '<div class="infoValue">' . $this->renderCounter() . '</div>';
            }else{
                $html .= '<img src="/img/'.$this->tile->Image.'">';
            }
            $html .= '</div>
                <div class="infoBoxRight">
                    <div class="infoTxt">
                        ' . $this->renderInfoText() . '
                    </div>
                    ' . $this->renderDetailLink() . '
                </div>
            </div>';

            $html .= '</div>';
        }

        return $html;
    }

    public function getOpenTile() {
        if(($this->company->StatusCode == 0 OR !isset($this->tile->ListValues)) AND $this->open != 'pub') {
            $html='';
        } else {
            $html = '<div class="description">
                 <div class="descTitle">
                     <div class="' . $this->tile->Image . '"></div>
                     <div class="ownerTitle"><h1>' . $this->renderTitle() . '</h1></div>
                </div>
                <div class="descText">' . $this->renderDescText() . '</div>
            </div>';
            if(isset($this->tile->ListValues) AND isset($this->tile->ListHead)) {
                $html .= $this->renderFullTable();
            }else{
                $html .= '<div class="description">';
                $html .= $this->renderTable();
                $html .= '</div>';
            }
        }
        return $html;
    }

    private function renderFullTable(){
        $html = '<div class="ownersTable">';

        $html .= '<div class="ownersRow ownersHeader">' . $this->renderTableHeader() . '</div>';

        $html .= $this->renderTable();

        $html .= '</div>';
        return $html;
    }

    private function renderDetailLink(){
        $params = ['company/tile', 'id' => $this->company->ChNr, 'name' => $this->company->FormatName, 'sub' => $this->tile->Sub];

        if(MH::GetLanguage()!='') {
            $params['lang'] = MH::GetLanguage();
        }

        return Html::decode(Html::a($this->tile->LinkText, $params , ['class' => 'btn infoBtn', 'onclick' => "dataLayer.push({'event':'onclick','event-category':'mhbeta_company','event-action':'click','event-label':'tile_".$this->tile->Sub."'});"]));
    }

    private function renderCounter(){
        if(strlen($this->tile->Counter)>3){
            return '<div style="font-size:24px;">'.$this->tile->Counter.'</div>';
        }
        return $this->tile->Counter;
    }

    private function renderDescText(){
        return $this->tile->Text;
    }

    private function handleTitleSize($title){
        if(strlen($title)>=14 AND strlen($title)<=16){
            return '<h2 style="font-size:14px;">'.$title.'</h2>';
        }
        elseif(strlen($title)>16 AND strlen($title)<=22){
            return '<h2 style="word-wrap:break-word;">'.$title.'</h2>';
        }
        elseif(strlen($title)>22){
            return '<h2 style="font-size:12px;word-wrap:break-word;">'.$title.'</h2>';
        }
        return '<h2>'.$title.'</h2>';
    }


    abstract function renderTitle();

    abstract function renderInfoText();

    abstract function renderTableHeader();

    abstract function renderTable();
}