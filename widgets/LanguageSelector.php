<?php

namespace app\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Url;
use app\controllers\MhController as MH;

class LanguageSelector extends Widget {

    public $params;

    private $availableLanguages = array('de','en','fr','it');

    public function run(){
        $html = $this->getLanguageLinks();
        echo $html;
    }

    public function getLanguageLinks(){
        $links = '';
        foreach($this->availableLanguages as $lang){
            if(MH::GetLanguage()=='' AND $lang=='de') {
                $links .= Html::a(strtoupper($lang), null, ['style' => 'color:#f0c060;', 'rel' => 'nofollow']);
            }elseif(strtolower(MH::GetLanguage()) == strtolower($lang)){
                $links .= Html::a(strtoupper($lang), Url::current(['lang'=>$lang]), ['style' => 'color:#f0c060;', 'rel' => 'nofollow']);
            }else {
                $links .= Html::a(strtoupper($lang), Url::current(['lang'=>$lang]), ['rel' => 'nofollow']);
            }
        }
        return $links;
    }
}