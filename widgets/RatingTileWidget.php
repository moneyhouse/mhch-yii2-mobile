<?php
namespace app\widgets;

use Yii;

class RatingTileWidget extends AbstractTileWidget {

    public function renderTitle(){
        return $this->tile->Title;
    }

    public function renderInfoText(){
        return $this->tile->InfoText;
    }

    public function renderTableHeader(){
        return '';
    }

    public function renderTable(){
        $html = '';
        $html .= '<div class="trackform">';
        $html .= '    <div class="alert alert-warning">';
        $html .= Yii::t('tileRating','Die von Ihnen gesuchte Seite ist noch nicht für Smartphones/Tablets verfügbar.');
        $html .= ' '.Yii::t('tileRating','Moneyhouse-Mobile befindet sich derzeit in der Beta-Phase und wird fortlaufend erweitert.');
        $html .= '<br><br>';
        $html .= Yii::t('tileRating','Über nachfolgenden Link gelangen Sie auf die nicht-mobile Seite.');
        $html .= '</div>';
        $html .= '    <a class="btn bottomBtn followBtn col-xs-12" href="https://www.moneyhouse.ch'.$this->tile->ListValues.'">'.Yii::t('tileRating','Weiter zur Bonitäts-Seite').'</a>';
        $html .= '    <div class="clearfix"></div>';
        $html .= '</div>';
        return $html;
    }

}