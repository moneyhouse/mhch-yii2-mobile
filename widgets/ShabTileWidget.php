<?php
namespace app\widgets;

use Yii;

class ShabTileWidget extends AbstractTileWidget {

    public function renderTitle(){
        return $this->tile->Title;
    }

    public function renderInfoText(){
        return $this->tile->InfoText;
    }

    public function renderTableHeader(){
        $head = $this->tile->ListHead;
        $header = '';
        if (!empty($head)) {
            $header .= '<div class="col-md-2 col-xs-4">' . $head->th1 . '</div>';
            $header .= '<div class="col-md-9 col-xs-12">' . $head->th2 . '</div>';
        }
        return $header;
    }

    public function renderTable(){
        $table = '';
        if(!empty($this->tile->ListValues)){
            foreach($this->tile->ListValues as $publication){
                $publ = $publication->Publication;
                $publ = str_replace($this->company->Name,'<b>'.$this->company->Name.'</b>',$publ);

                if(strlen($publ)>500){
                    $publ = substr($publ,0,500).' ...';
                }

                $table .= '<a class="ownersRow" rel="nofollow">';
                $table .= '    <div href="#" class="col-md-2 col-xs-4">SHAB '.substr($publication->PublId,4,3).'/'.substr($publication->PublId,0,4).'<br>vom ' . date('d.m.Y',strtotime($publication->ShabDate)) . '</div>';
                $table .= '    <div href="#" class="col-md-9 col-xs-12">' . $publ . '</div>';
                $table .= '</a>';
            }
        }
        return $table;
    }

}