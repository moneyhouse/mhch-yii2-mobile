<?php

namespace app\controllers;

//use app\controllers\MhController;

class SiteController extends MhController
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    public function actionIndex(){
        $this->layout = 'index';
        return $this->render('index');
    }
}
