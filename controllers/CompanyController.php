<?php
namespace app\controllers;

use app\services\CompanyService;
use app\services\TileService;

class CompanyController extends MhController
{
    private $company;

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    public function actionIndex($id){
        $id = $this->unformatId($id);
        $this->layout = 'main';

        return $this->render(
            'index',
            array(
                'company'=>$this->getCompany($id)
            )
        );
    }

    public function actionTile($id, $sub){
        $id = $this->unformatId($id);
        $this->layout = 'main';

        return $this->render(
            'index',
            array(
                'company'=>$this->getCompany($id),
                'tile' => $this->getTile($id,$sub),
                'subpage'=>$sub
            )
        );
    }

    private function unformatId($id){
        $id = str_replace('-','',$id);
        $id = str_replace('.','',$id);
        $id = str_replace('_','',$id);
        return $id;
    }

    private function getCompany($id){
        $companyService = new CompanyService();
        $companyService->getCompany($id);
        return $companyService->company;
    }

    private function getTile($id,$sub){
        $tileService = new TileService();
        $tileService->getTile($id,$sub);
        return $tileService->tile;
    }
}
