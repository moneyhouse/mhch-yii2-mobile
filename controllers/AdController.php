<?php
namespace app\controllers;

use Yii;
use yii\web\Controller;

class AdController extends Controller
{
    function __construct($id, $module=null)
    {
        parent::__construct($id, $module);
    }

    public static function getHeader($language){
        $siteId = self::getSiteId($language);

        $header = '';
        $header .= '<script src="//api.adquality.ch/bc.js"></script>';
        $header .= '<script type="text/javascript">';
        $header .= '	var BC_kv = new BusinessClass({';
        $header .= '		publisher_id: "5E485ADB-9EA0-4D5B-8994-A96FD920337D"';
        $header .= '	}).getKV();';
        $header .= '</script>';
        $header .= '<script type="text/javascript" src="http://aka-cdn-ns.adtech.de/dt/common/DAC.js"></script>';
        $header .= '<script type="text/javascript">';
        $header .= 'ADTECH.config.page = { protocol: \'http\', server: \'adserver.adtech.de\', network: \'1135.1\', siteid: \''.$siteId.'\', params: { loc: \'100\' }, kv: BC_kv};';
        $header .= '</script>';
        return $header;
    }

    public static function getAdTag($language){
        $placementId = self::getPlacement($language);

        $adtag = '';
        $adtag .= '<div class="container rectangleContainer">';
        $adtag .= '<div id="'.$placementId.'">';
        $adtag .= '    <noscript>';
        $adtag .= '        <a href="http://adserver.adtech.de/adlink|3.0|1135.1|'.$placementId.'|0|170|ADTECH;loc=300;alias=" target="_blank">';
        $adtag .= '            <img src="http://adserver.adtech.de/adserv|3.0|1135.1|'.$placementId.'|0|170|ADTECH;loc=300;alias=" border="0" width="300" height="250">';
        $adtag .= '        </a>';
        $adtag .= '    </noscript>';
        $adtag .= '</div>';
        $adtag .= '<script>';
        $adtag .= '     ADTECH.config.placements['.$placementId.'] = { sizeid: \'170\', params: { alias: \'\', target: \'_blank\' }};';
        $adtag .= '     ADTECH.loadAd('.$placementId.');';
        $adtag .= '</script>';
        $adtag .= '</div>';

        return $adtag;
    }

    private static function getSiteId($language){
        switch(strtolower($language)){
            case 'fr':
                return 781803;
            case 'it':
                return 781808;
            case 'en':
                return 781809;
            default:
                return 781781;
        }
    }

    private static function getPlacement($language){
        switch(strtolower($language)){
            case 'fr':
                return 5711246;
            case 'it':
                return 5711249;
            case 'en':
                return 5711254;
            default:
                return 5711160;
        }
    }
}