<?php
namespace app\controllers;

use Yii;
use yii\web\Controller;

class MhController extends Controller
{
    public $language;

    function __construct($id, $module=null)
    {
        parent::__construct($id, $module);
        $this->setLanguage();
        $this->language = $this->GetLanguageFromCountryCode(Yii::$app->language);
    }

    private function setLanguage() {
        if(isset($_GET['lang'])) {
            Yii::$app->language = $this->GetCountryCodeFromLanguage($_GET['lang']);
            $_SESSION['lang'] = $_GET['lang'];
        }
        else if (isset($_SESSION['lang']))
        {
            Yii::$app->language = $this->GetCountryCodeFromLanguage($_SESSION['lang']);
        }

        if(!isset($_SESSION['lang'])){
            $_SESSION['lang'] = 'de';
        }

        if(!isset(Yii::$app->language)){
            Yii::$app->language = $this->GetCountryCodeFromLanguage('de');
        }
    }

    function GetCountryCodeFromLanguage($language) {
        switch($language) {
            case "de":
                return "de-CH";
                break;
            case "de_":
                return "de-CH";
                break;
            case "en":
                return "en-US";
                break;
            case "fr":
                return "fr-CH";
                break;
            case "it":
                return "it-CH";
                break;
            default:
                return 'de-CH';
        }
    }

    public static function GetLanguage(){
        return self::GetLanguageFromCountryCode(Yii::$app->language);
    }

    private static function GetLanguageFromCountryCode($countryCode) {
        switch($countryCode) {
            case "de-CH":
                return "";
                break;
            case "en-US":
                return "en";
                break;
            case "fr-CH":
                return "fr";
                break;
            case "it-CH":
                return "it";
                break;
            default:
                return "";
                break;
        }
    }
}