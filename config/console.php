<?php
$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'mhbeta-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'app\commands',
    'modules' => [
        'gii' => 'yii\gii\Module',
    ],
    'components' => [
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => [
            'class'=>'yii\db\Connection',
            'dsn'=>'sqlite:/home/sm/web/mhyii2/mhbeta/database/dbbeta',
        ]
    ],
    'controllerMap' => [
        'translate' => \lajax\translatemanager\commands\TranslatemanagerController::className()
    ],
    'modules' => [
        'translatemanager' => [
            'class' => 'lajax\translatemanager\Module',
            'patterns' => '*.php',
        ],
    ],
    'params' => $params
];

return $config;