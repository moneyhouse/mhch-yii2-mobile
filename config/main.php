<?php
$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'mhbeta',
    'basePath' => dirname(__DIR__),
    'bootstrap' => [
        'log',
        'translatemanager'
    ],
    'language' => 'de-CH',
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is requi    red by cookie validation
            'cookieValidationKey' => '1jfv-Dtw4cSozSWnvIBpBwEhd_si2TRB',
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'translatemanager' => [
            'class' => 'lajax\translatemanager\Component'
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => false,
            'showScriptName' => false,
            'rules' => [
                '<lang:(fr|en|it)>/u/<sub>/<name>_<id>.htm' => 'company/tile',
                '<lang:(fr|en|it)>/u/<sub>' => 'company/tile',
                '<lang:(fr|en|it)>/u/<name>_<id>.htm' => 'company/index',
                '<lang:(fr|en|it)>/u/index' => 'company/index',
                '<lang:(fr|en|it)>/' => 'site/index',
                'u/<sub>/<name>_<id>.htm' => 'company/tile',
                'u/<sub>' => 'company/tile',
                'u/<name>_<id>.htm' => 'company/index',
                'u/index' => 'company/index',
                '/' => 'site/index',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>'
            ],
        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\DbMessageSource',
                    'db' => 'db',
                    'sourceLanguage' => 'de-CH',
                    'sourceMessageTable' => 'language_source',
                    'messageTable' => 'language_translate',
                ]
            ]
        ],
        'db' => [
            'class'=>'yii\db\Connection',
            'dsn'=>'sqlite:/home/sm/web/mhyii2/mhbeta/database/dbbeta',
        ]
    ],
    'modules' => [
        'translatemanager' => [
            'class' => 'lajax\translatemanager\Module',
            'patterns' => ['*.php'],
            'ignoredCategories' => ['yii','language','model','array'],
            'ignoredItems' => ['config']
        ],
    ],
    'controllerMap' => [
        'translate' => \lajax\translatemanager\commands\TranslatemanagerController::className()
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = 'yii\gii\Module';
}

return $config;

/*
 $params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '1jfv-Dtw4cSozSWnvIBpBwEhd_si2TRB',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = 'yii\gii\Module';
}

return $config;
 */