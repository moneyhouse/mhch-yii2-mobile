/**
 * Created by claudio1 on 12.05.15.
 */

open = false;
function hideShortInfoBox(){
    if($( window ).width()<=991){
        $(".companyAdress, .companyBottom, .emailTxt, .emailField, .followBtn, .monitoringMail").hide();
        $(".moreArrow").removeClass("moreArrowUp").show();
    }
}
function showShortInfoBox(){
    $(".companyAdress, .companyBottom, .emailTxt, .emailField, .followBtn, .monitoringMail").show();
    $(".moreArrow").hide();
}
$( window ).resize(function() {
    if($( window ).width()>991){
        if(!$(".companyAdress, .companyBottom, .emailTxt, .emailField, .followBtn, .monitoringMail").is(":visible")){
            showShortInfoBox();
        }
    }else{
        if(open != true){
            hideShortInfoBox();
        } else {
            $(".moreArrow").show();
        }
    }
});
if (($( window ).width()<=991) && typeof isOverview !== 'undefined' && isOverview) {
    $(".moreArrow").addClass("moreArrowUp").show();
    open = true;
} else {
    hideShortInfoBox();
}
$(".moreArrow").click(function(){
    if(!$(".companyAdress, .companyBottom, .emailTxt, .emailField, .followBtn, .monitoringMail").is(":visible")){
        $(this).addClass("moreArrowUp");
        $(".companyAdress, .companyBottom, .emailTxt, .emailField, .followBtn, .monitoringMail").show().addClass("animated fadeIn");
        open = true;
    }else{
        $(this).removeClass("moreArrowUp");
        $(".companyAdress, .companyBottom, .emailTxt, .emailField, .followBtn, .monitoringMail").hide().removeClass("animated fadeIn");
        open = false;
    }
});