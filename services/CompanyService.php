<?php
namespace app\services;

use Yii;
use app\controllers\MhController as MH;

class CompanyService extends AbstractService {
    public $company;

    public function getCompany($chnr) {
        //$apiuri = 'http://dev-sm.moneyhouse.ch/'.(MH::GetLanguage()==''?'de':MH::GetLanguage()).'/api/v1/company/'.$chnr;
        $apiuri = 'https://www.moneyhouse.ch/'.(MH::GetLanguage()==''?'de':MH::GetLanguage()).'/api/v1/company/'.$chnr;
        $this->company = self::getAPIContent($apiuri);

        $cChnr = $this->company->ChNr;
        $this->company->ChNr = 'CH-'.substr($cChnr,2,3).'.'.substr($cChnr,5,1).'.'.substr($cChnr,6,3).'.'.substr($cChnr,9,3).'-'.substr($cChnr,12,1);
        $this->company->FormatName = $this->RemoveSpecialChars(str_replace('&nbsp;','',strtolower($this->company->Name)));
    }

    private static function RemoveSpecialChars($str) {
        $spaceChar = '_';
        $lowercase = false;

        $str = utf8_decode($str);
        $str=trim($str);

        $str = str_replace('<br>',' ',$str);

        $_str = '';
        $i_max = mb_strlen($str);
        for ($i=0; $i<mb_strlen($str); $i++) {
            $ch = mb_substr($str, $i, 1);
            switch ($ch) {
                case 'Ä': case 'Æ':
                $_str .= 'A'; break;

                case 'ä': case 'æ':
                $_str .= 'a'; break;

                case 'à': case 'á':  case 'â': case 'ã':  case 'å':
                $_str .= 'a'; break;

                case 'À': case 'Á':  case 'Â': case 'Ã':  case 'Å':
                $_str .= 'a'; break;

                case 'Ç': case 'ç':
                $_str .= 'c'; break;

                case 'è': case 'é':  case 'ê': case 'ë':
                $_str .= 'e'; break;

                case 'È': case 'É':  case 'Ê': case 'Ë':
                $_str .= 'E'; break;

                case 'Ì': case 'Í':  case 'Î': case 'Ï':
                $_str .= 'I'; break;
                case 'ì': case 'í':  case 'î': case 'ï':
                $_str .= 'i'; break;

                case 'Ñ': case 'ñ':
                $_str .= 'n'; break;

                case 'Ö':
                    $_str .= 'O'; break;

                case 'ö':
                    $_str .= 'o'; break;

                case 'Ò': case 'Ó':  case 'Ô': case 'Õ':
                $_str .= 'O'; break;
                case 'ò': case 'ó':  case 'ô': case 'õ':
                $_str .= 'i'; break;

                case 'ß':
                    $_str .= 'ss'; break;

                case 'Ù': case 'Ú':  case 'Û':
                $_str .= 'U'; break;
                case 'ù': case 'ú':  case 'û':
                $_str .= 'u'; break;

                case 'Ü':
                    $_str .= 'U'; break;

                case 'ü':
                    $_str .= 'u'; break;

                case 'Ý':
                    $_str .= 'Y'; break;

                case 'ý': case 'ÿ':
                $_str .= 'y'; break;

                case 'Ð':
                    $_str .= 'D'; break;

                case ' ':
                    $_str .= $spaceChar;
                    break;
                case '.':
                    $_str .= $spaceChar;
                    break;
                case '/':
                    $_str .= $spaceChar;
                    break;
                case '\'':
                    $_str .= $spaceChar;
                    break;
                case '\\':
                    $_str .= $spaceChar;
                    break;
                case '_':
                    $_str .= $spaceChar;
                    break;
                case '-':
                    $_str .= '_';
                    break;
                case ':':
                    $_str .= '_';
                    break;
                case "'":
                    $_str .= '_';
                    break;
                case '"':
                    $_str .= '';
                    break;
                default :
                    if (mb_ereg('[A-Za-z0-9\(\)]', $ch)) { $_str .= $ch;  }
                    break;
            }
        }

        $_str = str_replace("{$spaceChar}{$spaceChar}", "{$spaceChar}", $_str);
        $_str = str_replace("{$spaceChar}-", '-', $_str);
        $_str = str_replace("-{$spaceChar}", '-', $_str);

        if (mb_substr($_str,0,1) == $spaceChar) {
            $_str = mb_substr($_str,1);
        }

        if($lowercase) {
            $_str = mb_strtolower($_str);
        }

        $_str = utf8_encode(trim($_str,$spaceChar));
        return $_str;
    }
}