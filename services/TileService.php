<?php
namespace app\services;

use Yii;
use app\controllers\MhController as MH;

class TileService extends AbstractService {
    public $tile;

    public function getTile($chnr,$sub){
        //$apiuri = 'http://dev-sm.moneyhouse.ch/' . (MH::GetLanguage() == '' ? 'de' : MH::GetLanguage()) . '/api/v1/company/' . $chnr . '/tile/' . self::getSubLink($sub);
        $apiuri = 'https://www.moneyhouse.ch/' . (MH::GetLanguage() == '' ? 'de' : MH::GetLanguage()) . '/api/v1/company/' . $chnr . '/tile/' . self::getSubLink($sub);
        $this->tile = self::getAPIContent($apiuri);
    }

    private function getSubLink($sub){
        switch($sub){
            case 'pub':
                return 'shab';
            case 'v':
                return 'functions';
            case 'p':
                return 'signatures';
            case 'fin':
                return 'share';
            case 'ra':
                return 'rating';
            default:
                break;
        }
    }
}