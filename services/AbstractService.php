<?php
namespace app\services;

abstract class AbstractService {

    protected function getAPIContent($apiuri)
    {
        //$cntnt = file_get_contents($apiuri);

        $curl_handle=curl_init();
        curl_setopt($curl_handle, CURLOPT_URL, $apiuri);
        curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
        $cntnt = curl_exec($curl_handle);
        curl_close($curl_handle);

        $json = json_decode($cntnt);
        return $json;
    }
}