<?php
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var string $name
 * @var string $message
 * @var Exception $exception
 */

$this->title = 'Seite nicht gefunden - moneyhouse';
?>

<div class="subsiteContent">
    <div class="container">
        <div class="infoTxt">

            <h1><?= nl2br(Html::encode($message)) ?></h1>
            <p><strong>Die von Ihnen angeforderte Seite konnte leider nicht gefunden werden.</strong>
            </p>

            <p>Die Seite ist entweder gelöscht oder verschoben worden. Möglicherweise haben Sie einen veralteten Link verwendet oder sich vertippt.</p>

            <p>Bitte überprüfen Sie:
            <ul>
                <li>die Schreibweise der URL</li>
                <li>die Seite, von der Sie gekommen sind</li>
            </ul>
            </p>

            <p>Ansonsten klicken Sie auf den "Zurück-Button" Ihres Browsers oder gehen Sie zur <?= Html::a('Startseite', ['site/index']); ?>, um zu unserem Angebot zurückzukommen.
            </p>

            <p>Danke für Ihr Verständnis!</p>
        </div>
    </div>
</div>