<?php

$this->title = Yii::t('general','Handelsregister- und Wirtschaftsinformationen').' - Moneyhouse';
//$this->registerMetaTag(['name' => 'google-site-verification', 'content' => '2CT4nyno0lnBaWDlfwVrc_SCeGDTsqd0Ap6tc3900YE']);
?>

<div class="container">
    <div class="infoTxt">
        <div class="row">
            <h1>Was ist Moneyhouse?</h1>
            <div class="col-sm-6">
                <h2>Über Moneyhouse</h2>
                <p>Moneyhouse ist das Internetportal für Wirtschaftsinformationen. Sie finden bei Moneyhouse schnell und unkompliziert aktuelle
                    Hintergrundinformationen zu Schweizer Unternehmen. Neben allgemeinen Handelsregisterdaten finden Sie finanzielle Kennzahlen,
                    Informationen zu Entscheidern und zum Umfeld eines Unternehmens.
                </p>
            </div>
            <div class="col-sm-6">
                <h2>Leistungen</h2>
                <p>
                    Bei Moneyhouse finden Sie Informationen zu allen im Handelsregister eingetragenen Unternehmen, inklusive Geschäftsführern, Vorständen sowie Prokuristen.
                    Darüber hinaus bietet Moneyhouse auch Finanzinformationen wie Bonitätsauskünfte, Bilanzen und Bilanzinformationen.
                </p>
                <p>
                    Neben den täglichen Veränderungen und Neueintragungen im Handelsregister bietet Moneyhouse auch...
                </p>
            </div>
        </div>
        <div class="row moreContainer">
        </div>
    </div>
</div>
<div class="clearfix"></div>