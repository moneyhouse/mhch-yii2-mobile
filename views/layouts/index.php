<?php
use yii\helpers\Html;
use app\assets\AppAsset;
use app\widgets\LanguageSelector;

AppAsset::register($this);

?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="noindex, nofollow">
        <link rel="icon" href="/favicon.ico">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
    <img style="display:none" src="http://moneyh.wemfbox.ch/cgi-bin/ivw/CP/<?=(isset($_SESSION['lang']) ? $_SESSION['lang']: 'de')?>?d=<?=rand(1000,100000)?>" alt="" width="1" height="1" border="0">
    <?php $this->beginBody() ?>
    <div class="homeTop">
        <div class="navigation">
            <div class="container">
                <div class="row">
                    <div>
                        <a class="logo" href="/"></a>
                        <div class="title"><?=Yii::t('general','Handelsregister- und Wirtschaftsinformationen') ?></div>
                    </div>
                    <div class="col-md-3 col-xs-4">
                        <div class="topNav">
                            <?= LanguageSelector::widget(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="jumbotron">
            <div class="container">
                <div class="row">
                    <form id='company-search' action="http://www.moneyhouse.ch/firmensuche.htm" method="get">
                        <div class="col-md-12">
                            <div class="input-group">
                                <input id="company-search-name" name="f" type="text" placeholder="<?=Yii::t('general','Unternehmen suchen')?>" class="form-control searchfield">
									<span class="input-group-btn">
									<button id="company-search-submit" type="submit" class="btn btn-default searchbutton"></button>
								</span>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="homeBottom">
        <?= $content ?>
    </div>

    <?= $this->render('elements/footer') ?>

    <?php $this->endBody() ?>

    </body>
    </html>
<?php $this->endPage() ?>