
<div class="footer">
    <div class="container">
        <div class="row">
            <div class="footerLogo"></div>
            <div class="mediaGroup">
                <?= Yii::t('general','Ein Unternehmen der {logo}',array('logo'=>'<img src="/img/medialogo.png">')) ?>
            </div>
        </div>
        <div class="row">
            <div class="copyright col-md-12">
                <?=Yii::t('general','Benötigen Sie Hilfe? E-Mail an {email} / 2015 Copyright Moneyhouse AG',array('email'=>'<a href="mailto:info@moneyhouse.ch">info@moneyhouse.ch</a>')) ?>
            </div>
        </div>
    </div>
</div>

<!-- Google Tag Manager -->
<noscript>
    <iframe src="//www.googletagmanager.com/ns.html?id=GTM-MWNJKZ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-MWNJKZ');
</script>
<!-- End Google Tag Manager -->