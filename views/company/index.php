<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\widgets\TileWidget;
use app\controllers\MhController as MH;
use app\controllers\AdController as Ads;

if(isset($tile) AND !is_null($tile->MetaTitle)){
    $this->title = $tile->MetaTitle;
}elseif(!is_null($company->MetaDescription)){
    $this->title = $company->Name.', '.$company->Address->City;
}

if(isset($tile) AND !is_null($tile->MetaDescription)){
    $this->registerMetaTag(['name' => 'description', 'content' => $tile->MetaDescription]);
}elseif(!is_null($company->MetaDescription)){
    $this->registerMetaTag(['name' => 'description', 'content' => $company->MetaDescription]);
    echo '<script type="text/javascript">var isOverview = true;</script>';
}

echo Ads::getHeader(MH::GetLanguage());
?>

<div class="container">
    <div class="row">
        <div class="col-md-4 col-xs-12">
            <div class="companyLeft">
                <div class="moreArrow" style="display: none;"></div>
                <div class="companyHead">
                    <?php
                    $urlParam = array();
                    $urlParam = ['company/index', 'id' => $company->ChNr, 'name' => $company->FormatName,];
                    if(MH::GetLanguage()!=''){
                        $urlParam['lang'] = MH::GetLanguage();
                    }
                    ?>
                    <a href="<?=Url::to($urlParam)?>"><div class="companyIcon <?=($company->StatusCode == 0 ? 'inactive' : '')?>"></div></a>
                    <div class="companyTitle">
                        <div class="companyName"><?= Html::a($company->Name, $urlParam, ['onclick' => "dataLayer.push({'event':'onclick','event-category':'mhbeta_company','event-action':'click','event-label':'company_name'});"]); ?></div>
                        <div class="companyStatus"><?=Yii::t('company','Status') ?>: <span class="<?= ($company->StatusCode == 0 ? 'statusInactive' : ($company->StatusCode == 3 ? 'statusSemiActive': 'statusActive')) ?>"><?=$company->Status?></span></div>
                    </div>
                </div>
                <div class="companyAdress">
                    <?php
                    if(!is_null($company->Address)) {
                        echo '<div class="adressLine">' . $company->Address->Street . '</div>';
                        echo '<div class="adressLine">' . $company->Address->Zip . ' ' . $company->Address->City. '</div>';
                    } else {
                        echo '<div class="adressLine">-</div>';
                    }
                    ?>

                    <div class="adressLine">Schweiz</div>
                </div>
                <div class="companyBottom">
                    <div class="companyRow">
                        <div class="colLeft"><?=Yii::t('company','Rechtsform') ?></div>
                        <div class="colRight"><?= $company->LegalForm ?></div>
                    </div>

                    <div class="companyRow">
                        <div class="colLeft"><?= Yii::t('company','Kapital') ?></div>
                        <div class="colRight"><?= (!is_null($company->Share)?number_format($company->Share):'-') ?></div>
                    </div>

                    <div class="companyRow">
                        <div class="colLeft"><?= Yii::t('company','Handelsregister-Nr') ?></div>
                        <div class="colRight"><?= (!is_null($company->ChNr)?$company->ChNr:'-') ?></div>
                    </div>

                    <div class="companyRow">
                        <div class="colLeft"><?= Yii::t('company','UID') ?></div>
                        <div class="colRight"><?= (!is_null($company->UID)?substr($company->UID,0,3).'-'.substr($company->UID,3,3).'.'.substr($company->UID,6,3).'.'.substr($company->UID,9,3):'-') ?></div>
                    </div>

                    <div class="companyRow">
                        <div class="colLeft"><?= Yii::t('company','Handelsregisteramt') ?></div>
                        <div class="colRight"><?= (!is_null($company->Address->Canton)?Yii::t('general','Kanton').' '.$company->Address->Canton:'-') ?></div>
                    </div>

                    <div class="companyRow">
                        <div class="colLeft"><?= Yii::t('company','Rechtssitz der Firma') ?></div>
                        <div class="colRight"><?= (!is_null($company->Place->Town)?$company->Place->Town.' ('.$company->Place->Canton_Short.')':'-') ?></div>
                    </div>

                    <div class="companyRow">
                        <div class="colLeft"><?= Yii::t('company','Eintragung') ?></div>
                        <div class="colRight"><?= (!is_null($company->InscriptionDate)?date('d.m.Y',strtotime($company->InscriptionDate)):'-') ?></div>
                    </div>
                </div>
            </div>
            <?php echo Ads::getAdTag(MH::GetLanguage()); ?>
            <?php
                echo '<div class="overviewText">' . $company->Purpose . '</div>';
            ?>
            <div class='formContainer'>
            </div>
        </div>
        <div class="col-md-8 col-xs-12">
            <div class="companyRight">
                <?php
                if(isset($tile)){
                    echo TileWidget::widget(['tile'=>$tile,'company' => $company, 'open' => $subpage]);
                }
                ?>
                <div class="infoBoxes">
                    <?php
                    $countOpenTiles = 0;
                    for($i = 0; $i < count($company->Tiles); $i++){
                        $smallTile = $company->Tiles[$i]->Tile;
                        if(isset($subpage) AND $smallTile->Sub == $subpage){
                            $countOpenTiles++;
                            continue;
                        }
                        echo \app\widgets\TileWidget::widget(['tile'=>$smallTile, 'company'=>$company, 'second' => (($i + $countOpenTiles) % 2 == 0 ? false : true)]);
                    }
                    ?>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>